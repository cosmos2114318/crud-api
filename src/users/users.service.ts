import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import {UserDto} from './users.dto'


export interface User {
    id: string;
    username: string;
    age: number;
    hobbies: string[];
}

@Injectable()
export class UsersService {
    private user: User[] = [];
  
    create(item: UserDto): User {
      const newItem = { id: uuidv4(), ...item };
      this.user.push(newItem);
      return newItem;
    }
  
    findAll(): User[] {
      return this.user;
    }
  
    findOne(id: string): User {
      const item = this.user.find((item) => item.id === id);
      if (!item) {
        throw new NotFoundException(`Item with ID ${id} not found`);
      }
      return item;
    }
  
    update(id: string, updateData: UserDto): User {
      const item = this.findOne(id);
      Object.assign(item, updateData);
      return item;
    }
  
    remove(id: string): void {
      const index = this.user.findIndex((item) => item.id === id);
      if (index === -1) {
        throw new NotFoundException(`Item with ID ${id} not found`);
      }
      this.user.splice(index, 1);
    }
}