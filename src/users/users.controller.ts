import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    ParseUUIDPipe,
    UsePipes,
    ValidationPipe,
  } from '@nestjs/common';
  import { UsersService } from './users.service';
  import {UserDto} from './users.dto'

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

  @Post()
  @UsePipes(new ValidationPipe({ transform: true, forbidUnknownValues: true })) // Apply ValidationPipe
  create(@Body() createUserDto: UserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseUUIDPipe) id: string) {
    return this.usersService.findOne(id);
  }

  @Put(':id')
  @UsePipes(new ValidationPipe({ transform: true, forbidUnknownValues: true })) // Apply ValidationPipe
  update(
    @Param('id', ParseUUIDPipe) id: string,
    @Body() updateUserDto: UserDto,
  ) {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id', ParseUUIDPipe) id: string) {
    this.usersService.remove(id);
  }
}
