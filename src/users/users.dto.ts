import { IsDefined, IsString, IsArray, IsNotEmpty, ArrayNotEmpty, IsInt, Max  } from 'class-validator';

export class UserDto {

    @IsDefined()
    @IsString()
    @IsNotEmpty()
    username: string;
  
    @IsDefined()
    @IsInt()
    age: number;

    @IsArray()
    @IsDefined()
    @ArrayNotEmpty()
    @IsString({ each: true })
    hobbies: string[];

}